
locals {
  repositories = [
    {
      name        = "gcp-data-infrastructure-1-base-infrastructure"
      description = "This repository houses Terraform code for creating native GCP resources, including BigQuery and Cloud Composer, forming the foundational infrastructure for the data platform."
    },
    {
      name        = "gcp-data-infrastructure-2-airflow-dags"
      description = "This repository contains Airflow pipelines. The recommended approach is to utilize the KubernetesPodOperator and Docker images created from other repositories. These pipelines manage data workflows efficiently within the GCP environment."
    },
    {
      name        = "gcp-data-infrastructure-3-data-imports"
      description = "This repository hosts Docker images used for data imports. These images facilitate seamless and efficient importing of data into the GCP data infrastructure, ensuring data availability and reliability."
    },
    {
      name        = "gcp-data-infrastructure-4-dbt"
      description = "This repository encompasses dbt (data build tool) code, models, and tests. It serves as the hub for data transformation logic, enabling data engineers and analysts to build and validate data models effectively within the GCP data environment."
    }
  ]
}

resource "gitlab_project" "gitlab_project" {
  for_each = { for idx, repo in local.repositories : idx => repo }

  name             = each.value.name
  description      = each.value.description
  visibility_level = "public" # Set repositories as public, change to "private" for private repositories
}
